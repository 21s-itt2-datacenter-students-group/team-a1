# Current

Open loop current sensors consist of a Hall sensor mounted in the air gap of a magnetic core (Fig. 1). A conductor produces a magnetic field comparable to the current. The magnetic field is concentrated by the core and measured by the Hall sensor. The signal from the Hall generator is low, so it is amplified, and it is this amplified signal that becomes the sensor’s output. Open-loop sensors normally have circuitry that provides temperature compensation and calibrated high-level voltage output. While they have a definite price advantage over closed-loop counterparts, their downside is that they can be prone to saturation and temperature drift. The drift can be minimized to some extent, however, by injecting a positive coefficient in the control current to reduce the drift in sensitivity over temperature.

- analog output, which duplicates the wave shape of the sensed current.
- bipolar output, which duplicates the wave shape of the sensed current.
- unipolar output, which is proportional to the average or RMS value of the sensed current.
- Hall sensor measures magnetic field of the current 
- useful to measure current without physically connecting to the system                               
- Can measure distance as well


# Temperature
#### Thermocouple
- A thermocouple is a sensor that consists of two dissimilar metal wires, joined at one end, and connected to a thermocouple thermometer or other thermocouple-capable device at the other end.
- When two wires composed of dissimilar metals are joined at both ends and one of the ends is heated, there is a continuous current which flows in the thermoelectric circuit
- when the junction of the two metals is heated or cooled a voltage is produced that can be correlated back to the temperature.
- 2-3 times cheaper than RTDs

#### Isobar

- Standard mercury Thermometer
#### Resistance Temperature Detectors (RTDs)
- RTDs work on a basic correlation between metals and temperature. As the temperature of a metal increases, the metal's resistance to the flow of electricity increases.
- Platinum is the most commonly used metal for RTD due to its chemical inertness, nearly linear temperature versus resistance relationship, temperature coefficient of resistance that is large enough to give readily measurable resistance changes with temperature and stability 
#### Thermistors

- A thermistor is a resistance thermometer, or a resistor whose resistance is dependent on temperature. The term is a combination of “thermal” and “resistor”. It is made of metallic oxides, pressed into a bead, disk, or cylindrical shape and then encapsulated with an impermeable material such as epoxy or glass.

# Humidity
- Capacitive, utilizing that a small change in humidity of the dielectric material causes a large change in capacitance to measure the humidity, cheap. durable but accuracy isn’t great. 
- Gravitational, most precise Hygrometer but expensive, impractical and hard to calibrate
- Resistance, measuring changing resistance in a salt or polymer caused by humidity, less accurate than capacitive and since the resistance also depends on the temperature needs to be combined with a thermometer
- Thermal, measures the thermal conductivity of the air as a function of humidity, measures absolute humidity.

# Airflow
- Mass flow sensor
- reverse fan?
- https://sps.honeywell.com/us/en/products/sensing-and-iot/sensors/airflow-sensors
- airflow sensors operating on heat transfer — flow and differential pressure
- air pressure(membrane)
- thermometers at key spots?


