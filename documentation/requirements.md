# Requirements

## Components 
### Hardware
- Temperature sensors
- Humidity sensors
- Raspberry Pi
- Cables / wires
- LEDs
- Screen to show data
- ESP8266
### Software
- SQLite
- Custom GUI (PyQt ?)
### Protocols:
- Coding Language
- Python
- SQL
- MQTT

