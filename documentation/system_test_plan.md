## **System test plan**

### Step 1. **Raspberry Pi**
**Problem:** Accidental power cycle. 
<br>
**How could it be solved?:** Make sure the RPi is able to launch the programs critical for the system, on boot.
<br>
** Solution:** Check if program(s) are running via. another program, if not start them via. the program

**Problem:** Code crash 
<br>
**How could it be solved?:** Check if code contains errors (Syntax errors, missing libraries etc.)
<br>
** Solution:** Have all the functionality for the code being unit tested and run through a GitLab pipeline to check for any potential errors/crashes. Have a watchdog program checking for, if the program is still running.

**Problem:**  Network error 
<br>
**How could it be solved?:**
<br>
** Solution:** Check for internet connection, and maybe add functionality for the system to try to reconnect after a specific amount of time
Attempt to gain internet access using another AP or device

**Problem:** Code doesn’t run on boot 
<br>
**How could it be solved?:** Manual check to then start the code
<br>
** Solution:** Implement watchdog program to check if code runs, if not, start the program and check that it runs successfully.

### Step 2. **Sensor 1 & 2 (DHT11)**
**Problem:** Faulty connection. 
<br>
**How could it be solved?:** Have the program print data to a terminal that can be observed on the rpi
<br>
** Solution:** Start by checking that software is not the cause of error, afterwards check physical connections to sensor.



**Problem:** Inaccurate readings
<br>
**How could it be solved?:** Compare readings to the other temperature sensor
<br>
** Solution:** Implement method to code, if values from one sensor fluctuates too much, then have it compared to the second sensor to determine if there's a problem.



### Step 3. **Sensor 3 (FCS-01)**
**Problem:** Faulty connection 
<br>
**How could it be solved?:** Manual check on the component every once in a while.
<br>
** Solution:** Start by checking that software is not the cause of error, afterwards check physical connections to sensor.



### Step 4. **MQTT Broker**
**Problem:** Loss of connection(someone blocking the signal or something goes wrong with the sender) 
<br>
**How could it be solved?:** Automatic test that pings the server to ensure there is still connection, attempt to access the server from another client
<br>
** Solution:** Make sure loop method is enabled for the MQTT Client, this will automatically make the client retry to connect to the broker.


### Step 5. **Node-red**
**Problem:** Buggy code. 
<br>
**How could it be solved?:** Double check the code :)
<br>
** Solution:** Have code be reviewed by other team members to make sure code doesn't contain errors, also unit testing of the code could potentially sort out bugs.


**Problem:** Data not going to the right visuals 
<br>
**How could it be solved?:** Manual test via direct input to see if things go to the right place
<br>
** Solution:** Have dashboard elements reviewed by other team members to make sure everything looks and works correctly.
