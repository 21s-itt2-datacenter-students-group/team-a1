## Potential requirement areas for sensors in a datacenter 

- Precise / accurate, high resolution range
- Doesn’t require daily calibrations to maintain accuracy
- Able to work under required temperature conditions (10 degrees - 80 degrees MAX)
- Waterproof / moisture proof (40%-60% relative humidity)
- Small enough to fit into tight places
- Complete modules (rather than creating the modules ourselves from the sensors)
- Uncertainty of measurement doesn’t exceed 5%
- Cheap enough to replace
- Power consumption


Application | Location of sensors | Precision/ accuracy
| ------ | ------ | ------ |
Ambient Room  Humidity | Depending on size of the room: close to the door, center of room, center of racks and furthest point from door. | 40% - 60% rH. (relative humidity)
Ambient Room Temperature. | small rooms: center. data centers: potential hot zones. | 18-27°C 
Rack Level Monitoring Sensors to monitor intake temperature | Under data rack | 18-27°C / 64-80°F
Sensors to monitor outtake temperature | over data rack | no more than intake temperature + 20°C/K
