## Group Roles

### Scrum Master Role Description

The scrum masters role is to lead the scrum daily meetings. They should maintain harmony within the group and make sure that everyone's opinion gets heard. The Scrum Master teaches the group to keep the Daily Scrum within the 15-minute time-box and ensures that the meeting does not exceed the time limit.

### Facilitator Role Description

The facilitator creates a plan/ guide and manages a group or event to ensure that the group's objectives are met effectively, with clear thinking, good participation and full effort from everyone who is involved

### Secretary Role Description

The secretary's role is to take notes during the meetings or activities held. The notes should be documented for the group to look back on at a later date. The notes should cover what important topics and things have been discussed in chronological order. 


## Team Role Rotation Plan

| Scrum Master & Facilitator | Week | Secretary Role |
| ------ | ------ | ------ |
| Reuben Badham (@Reuben_Badham) | Monday | Rudi Hansen (@rohh28794) |
| Jakob Dahl (@JBRD99) | Wednesday | Reuben Badham (@Reuben_Badham) |
| Christian Dalsgard (@chrid1909) | Monday | Jakob Dahl (@JBRD99) |
| Lauge Mathiesen (@lauge.mat) | Wednesday | Christian Dalsgard (@chrid1909) |
| Mathias Knudsen (@mhjk28842) | Monday | Lauge Mathiesen (@lauge.mat) |
| Rudi Hansen (@rohh28794) | Wednesday | Mathias Knudsen (@mhjk28842) |
