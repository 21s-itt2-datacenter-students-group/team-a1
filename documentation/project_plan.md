# Background

This is the semester ITT2 project where we will work with different projects, initiated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

# Purpose

The main goal is to have a system where IoT sensors collect data from a datacenter.
This is a learning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated will revolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

# Goals

The overall system that is going to be build looks as follows:  
![project_overview](https://eal-itt.gitlab.io/datacenter-iot/assets/images/index/datacenter_iot_system_overview.png)![](https://user-content.gitlab-static.net/a3ec96b73e3767be870bc07be8de54690989f582/68747470733a2f2f6461746163656e746572322e6769746c61622e696f2f6461746163656e7465722d7765622f696d616765732f70726f6a6563745f6f766572766965775f6461746163656e746572322e706e67)

Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: 


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Documentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 
* Presentation
* TBD: Fill in more, as agreed, with teams from the other educations

# Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

# Organization

* Steering committee
    -  Lecturers<br><br>
* Project managers
    - February: Christian Dalsgard & Reuben Badham<br><br>
* Project groups
    - Team A1<br><br>
* External stakeholders
    - Future employers or partners<br><br>
# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute,


# Risk assessment

1. Someone didn’t do their job
    - Weekly reports about progress could indicate problems
    - Checkups during team meetings
    - Lack of focus during: Work, Meetings etc.
    - Failure to attend.<br><br>    
2. Hardware Malfunction/errors
    - Redundancy
    - Troubleshooting
    - Incorrect specifications/information
    - Problems implementing our prototype in a real environment<br><br>
3. Miscommunication
    - Team meetings to establish proper communication
    - Make sure everyone understands what is being said<br><br>
4. Mismanagement of time
    - Proper planning for example microsoft project, calendars and documents.<br><br>
5. Documentation
    - Make sure you do it :-)<br><br>
6. Too ambitious
    - Stick to the plan
    - K.I.S.S. Keep it simple, Stupid!
    - losing overview of the project.<br><br>
7. Sickness / Other injuries
    - In case of sickness / injuries, make sure work is documented and delegated.
    - Communicate!
    - Corona Issues<br><br>
8. Lack of technical knowledge
    - Google, reading, ask your team and lecturers.
    - Bad Code
    - Ask your team, google and documentation
    - Incorrect advice from teacher/lectures<br><br>
9. Didn’t ask for help
    - Don’t be shy to ask for help, no matter the problem/question.
    - Not asking enough questions to the team and teachers.<br><br>
10. Personal Complications/Disagreements
    - Should a minor interpersonal conflict erupt between team members they should work it out in private between themselves
    - Should a major interpersonal conflict erupt between team members the team should come together and discuss, possibly with a 3rd party mediator<br><br>
11. Bad planning
    - Plan thoroughly
    - Have someone analyze the plan
    - Don’t be afraid to change the plan
    - Project gets canceled<br><br>
12. Didn’t have a plan
    - Talk with lecturers about how to properly plan<br><br>
13. Misunderstood the project
    - In team meetings, have focus and ask questions about the parts of the project you don’t understand.
    - miscommunication, assumptions<br><br>
14. No backups
    - Make backups to third party storage services such as Google Drive, Dropbox etc.<br><br>
15. Lost interest of project
    - Make sure everyone is involved in planning the project, so that we make sure everyone at least finds it a bit interesting.
    - If a struggling team-member lags behind and they aren't caught up, it may lead to overworking of the rest of the group.<br><br>
16. Afraid to speak up about problems (Hardware issues, etc)
    - Try and speak up, have better team relations.<br><br>
17. Outside interference<br><br>
# Stakeholders

- Us
    - Communication: Discord / Messenger / Element / Physical / Email
    - Interests: Education / Training / Learning / Money


- Potential customers
    - Communication: GoFundMe / Email
    - Interests: Working product / Easing of potential problems in company / Cost savings


- Teachers
    - Communication: Element / Discord / Email / Physical
    - Interests: Successful students / reputation


- Potential employers
    - Communication: LinkedIn / Physical / Email
    - Interests: innovation and developing new prototypes 


There are no diverse interests in our stakeholders.


# Perspectives

This project will serve as a template for other similar projects in future semesters.
 
It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.
 
It will serve as a valuable lesson in working on a real IT project.

# Evaluation

Group hand-in and a presentation.

Based upon whether we get a passing grade or getting gainfully employed by potential customers

Also, later analyzing our workflow and work done, in a certain amount of time, we can evaluate whether or not the quality of the work is satisfactory.


# References

- https://eal-itt.gitlab.io/21s-itt2-project/
- https://masterofproject.com/blog/3813/scrum-sprint-backlog 
- https://www.scrum.org/resources/what-is-scrum


## Common project group on gitlab.com

 The project is managed using gitlab.com. 
 
 The groups is at [https://gitlab.com/21s-itt2-datacenter-students-group](https://gitlab.com/21s-itt2-datacenter-students-group)

