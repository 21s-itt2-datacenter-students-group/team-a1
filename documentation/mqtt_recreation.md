- ## MQTT Code
    - ### Publish
        Publishing to your MQTT broker of choice is an easy task. The code just needs to follow some guidelines and you’ll be good to go.

        [**Code**](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/mhjk-stuff/mqtt-publish.py)

        This code is partly original and partly from Nikolaj Simonsen. 
        This code is mainly used to create the payload as a dictionary, and convert it to json format to be sent to a MQTT broker. 
        This code also allows you to subscribe and display messages, which is a way for you to confirm that the message is coming through. 
        The payload created is a placeholder for sensor information and the code has some redundant code for when I used it to send thermal readings. 
        For now it sends “Hello there” as well as the current local time to a personal MQTT broker.

        To use this code, you will have to replace the variables, “client_name” and “broker_addr”. 
        This is crucial since the client name should always be unique and the broker address is currently filled in with my personal broker, which is never likely to be available unless I’m shortly testing with it, and any other public IP address other than my own is not likely to be whitelisted on my broker. 
        There are several free test brokers you can use online.
        The rest of the variables should probably be changed too, but for the sake of quick use, you can leave “sensor_id”, “topicSub”, “topicPub” and “publish interval”. 
        Though the “broker_port” should be changed to an available port and the topics should be changed to something that you’ll be more comfortable with using.


    - ### Subscribe
- ## Setting up the microservices
    - ### MQTT Broker
        - #### Setting up the virtual machine
        Sign into Azure / create an account.   
        In the top right, click on the Azure portal Button next to the sign in button.
        <br>  
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MQTTSetupPic1.png"/>
        <br>
        At the top of the page, unter the topic of “Azure services”, click on Virtual machines  
        <br>
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic2.png"/>  
        <br>
        Click on the “+” button labeled “add” to create a new virtual machine. Then click “Virtual machine. 
        <br>
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic3.png"/>
        <br>
        Now the basics tab is open in the “Create a virtual machine page.    
        Under Project details - subscription, select “FreeTrial”   
        For the Resource group, select the blue “create new” button and call it “myResourceGroup”.   
        The project details section should look like this.   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic4.png"/>
        <br>
        In the “Instance details” section, set the virtual machine name to be “myVM”      
        The region is set to “(US) East US” by default. If it wasn't set as default, then select it.   
        In availability options, keep “No infrastructure redundancy required” as default.    
        For the image select “Debian 10 “Buster” - Gen1”   
        The size should be “Standard B1s (1 vcpus, 1 GiB memory)”    
        The filled in section should look like this.    
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic5.png"/>
        <br>
        Under the “Administrator account” section, select SSH public key   
        Set the username to “azureuser”   
        In SSH public key source select “Generate new key pair”   
        For “Key pair name” type “myVM_key”   
        <br> 
        In the “inbound port rules” select “Allow selected ports” and select the SSH (22) for the “inbound port”.   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic6.png"/>
        <br>
        Click “Review + create”   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic7.png"/>
        <br>
        When this window pops up, click “download private key and create resource”   
        Save the .pem file somewhere it can be easily found.   
        Once complete, select “Go to resource”    
        You will see the virtual machine and the details for it.    
        Select the IP address and copy it to the clipboard.    
        For example -   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic8.png"/>
        <br>
        To SSH into the virtual machine open PowerShell prompt.    
        Type -   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic9.png"/>
        <br>
        Replace the file path with the location of the ssh key / .pem file saved from before.    
        Also replace the ip with the correct IP as stated in the above picture.    
        <br>
        Install the web server.    
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic10.png"/>
        <br>
        Once complete type exit to leave the SSH session.    

        - #### Setting up Mosquttio
        Add the mosquitto debian repository.    
        <br>
        wget http://repo.mosquitto.org/debian/mosquitto-repo.gpg.key   
        sudo apt-key add mosquitto-repo.gpg.key   
        cd /etc/apt/sources.list.d/   
        sudo wget http://repo.mosquitto.org/debian/mosquitto-jessie.list   
        sudo apt-get update   
        <br> 
        Install mosquitto and its command line clients   
        <br>  
        sudo apt-get install mosquitto   
        sudo apt-get install mosquitto-clients   
        <br>  
        Check that it is running   
        <br>  
        sudo service mosquitto status   
        <br>   
        You should see something like this   
        <br> 
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/7052d8f732d9c3fa2f0cb4010f90c02d21acc680/images/MQTTSetupPic11.png"/>
        <br>
        To start stop and restart the broker use these commands
        <br>
        sudo service mosquitto start
        sudo service mosquitto stop
        sudo service mosquitto restart
        <br>
        <br>
        Test that the broker is working 
        In the terminal type the following to subscribe to the “testtopic”
        <br>
        mosquitto_sub -h localhost -t "testtopic" -v
        <br>
        Then open another terminal and send a message on that topic. 
        <br>
        mosquitto_pub -h localhost -t "testtopic" -m "Testing"
        <br>
        If it works then something like this should show up. 
        <br>
        chip@chip:~$ mosquitto_sub -h localhost -t "testtopic" -v
        testtopic Testing
        <br>


        - #### Testing the broker
    - ### MQTT to MongoDB
        - #### Setting up Atlas/MongoDB account
            - ##### Creating the Atlas Account
            - ##### Deploying the cluster
            - ##### Whitelisting your IP Address
            After having deployed our clusters we would like to being able to access them. To do this we go to security on the main menu
                <br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic7.png"/>
                <br>
            Next step once we see the page below is to add an IP address
				<br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic8.png"/>
                <br>
            After you click the button a pop-up window will appear where you can insert the IP you want to whitelist and even whether you would like the access to only be temporary.
				<br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic9.png"/>
                <br>					
            Once you have entered the IP press confirm and you can now access the database from the IP address.
            - ##### Creating the database user for the cluster
            To create a user to the database you select database access on the list
				<br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic10.png"/>
                <br>
            Then you select to add a user
				<br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic11.png"/>
                <br>
            For the next step you fill out the overlay that appears with the type of authentication the user will use, the authority that the user will have and finally if the user should be temporary or not
				<br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/MongoDBPic12.png"/>
                <br>
            - ##### Testing your cluster
                - ###### Connecting to the cluster via. Mongo shell
                    1. First click on **connect** on the cluster you want to connect to.
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc1.png"/>
                        <br>

                    2. Then click on **Connect with the mongo shell.**
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc2.png" width = "65%"/>
                        <br>

                    3. For **step 1** in the guide select your operating system and download the latest version of **mongo shell** and unpack the folder to a location of your choice.
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc3.png" width = "50%"/>
                        <br>

                    4. Press **Windows + R** to open the run window and enter the following:
                        <br>                    
                        > rundll32.exe sysdm.cpl,EditEnvironmentVariables
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc4.png" width = "35%"/>
                        <br> 

                    5. Then add the path of your mongo shell bin folder to the **Path variable** under **User variables**.
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc5.png" width = "35%"/>
                        <br>

                    6. Enter the **connection string** shown in the guide and press enter.
                        <br>
                        Enter your password when it prompts for you to enter your password.
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc6.png" width = "75%"/>
                        <br>

                    7. You should by now have successfully connected to a desired database of your choice and you can now insert or view data which is shown in the following topic.

                - ###### Inserting and viewing data in the cluster
                    1. First we have to select the database we want to insert and view data in by typing the following command in a command prompt window: **use myDatabase**
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc7.png" width = "65%"/>
                        <br>
                        Instead of **myDatabase** you can type the name of an existing database in your cluster or an entirely new one.

                    2. Now we can use the following command example to insert a new record into a collection:
                    
                        db.people.insert({<br>
                        name: { first: "Alan", last: "Turing" },<br>
                        birth: new Date('Jun 23, 1912'),<br>
                        death: new Date('Jun 07, 1954'),<br>
                        contribs: [ "Turing machine", "Turing test", "Turingery" ],<br>
                        views : NumberLong(1250000)<br>
                        })
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc8.png" width = "65%"/>
                        <br>
                        After the command has been executed, you should see the following output:
                        <br> 
                        WriteResult({ "nInserted" : 1 })

                        This indicates that the document/item has been successfully inserted into the collection.

                    3. Now to view the data, we will type in the following command example: 
                        <br>
                        **db.people.find({ "name.last": "Turing" })**
                        <br>
                        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/tyc9.png" width = "75%"/>
                      
                        After the command has been executed, you should see the record we just had inserted into the collection.


        - #### Setting up the virtual machine
        - #### Setting up network
        - #### Cloning code & installing requirements
            1. When connected to your **MQTT to MongoDB** VM, we’ll start out by cloning the repository containing the python code that will be running on this VM as a microservice.

            2. By default your VM shouldn’t have **git** installed, therefore we enter the following command: 
                <br>
                **sudo apt install git** to install **git**

                Afterwards type: **git clone \<clone link to your chosen repo>**
                <br>
                <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/ccir1.png" width="65%"/>
                <br>
                When done, the output should look something like shown in the screenshot above.

            3. When the repository has been cloned, we’ll also need to install **pip** to install the required libraries for our python code

                Type the following command:
                <br>
                **sudo apt install python3-pip**

            4. Now we can install the libraries, which is done by entering the following command:
                <br>
                **pip3 install paho-mqtt pymongo dnspython**

            5. By now you should be able to run your mqtt subscriber code. 

- ## Troubleshooting
    - ### Error when receiving message (Subscribe)
        If you encounter an error when receiving the message in “mqtt-to-db.py” code, so graciously given to us by Nikolaj Simonsen and Mathias Gregersen, start by examining the publishing code. 

        Is the payload made into a json format? 
        Is what you’re converting into json even a dictionary? 

        If the code runs without crashing, but you can’t get a connection through to your DB, you should try restarting the code. Otherwise, make sure your cluster connection string is correctly formatted in the code. You will have to take the first part of your string and put it before the “token” part. Then you’ll have to take the part after <password> and put it after the token part. 
        Make sure you have a seperate .txt file in your directory from where you run the code given to us, which is called token.txt. It should include only your password with no angle brackets. Double check that the cluster connection string is wholly correct.
        
        'mongodb+srv://dbUser:' + token + '@examples.1qumu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'


        Otherwise, double check your public IP is allowed on your cluster. Your public IP might be refreshed by your ISP and will no longer be matching the one on mongodb.

    - ### MQTT-to-DB doesn’t receive messages
        If you encounter something alike shown in the below screenshot, which for me was receiving the message that the subscriber mqtt was connected twice, it may be highly likely that you’re connecting to the broker with a client id which is already in use.
        <br>
        <img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/t2.png" width="65%"/>
        <br>
        So the solution is to make sure you assign a unique client id to each subscriber/publisher.

        To quote a blog post from hivemq: 
        > The broker uses the ClientId to identify the client and the current state of the client. <br>
        Therefore, this Id should be unique per client and broker.

