## Available Suitable Sensors

| Type | Sensor | Measuring Technique | Pros | Cons | Cost | Link |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| Temperature | ZTP-135SR | measures temperature | Small size, Included thermistor for ambient comparison, High sensitivity, Fast response time, Low cost | its bad | 43,08 dkk | https://dk.farnell.com/amphenol-advanced-sensors/ztp-135sr/thermopile-ir-sensor-20-degree/dp/2506255 |
| Temperature | 287-18000 | Sensing | Cheap | Chinese Delivery time | 48,50 dkk | https://dk.farnell.com/mcm/287-18000/thermometer-type/dp/2830978 |
| Temperature | 44/830/7 | Isonbar | VERY CHEAP and Old people compatible | Physical, No digital data measurement | 14,72 dkk | https://dk.farnell.com/brannan/44-830-7/thermometer-glass-10-to-110deg/dp/3403902 |
| Temperature | 010010TD | Tubular ceramic encased Pt100 elements resistance dependant of temperature | High range, min and max temperature | Very expensive | 208,64 dkk | https://dk.farnell.com/labfacility/010010td/sensor-pt100-4wire-5x35mm/dp/7255743?st=temp |
| Temperature + Humidity | RHT 03 (SEN-10167) | capacitive humidity sensing digital temperature and humidity module | Includes a temperature and humidity sensor | Somewhat expensive, Ultra-low power, the transmission distance, fully automated calibration | 96,00 dkk | https://dk.farnell.com/mcm/83-17985/dht22-temp-humidity-sensor/dp/2801405?st=dht22 |
| Pressure | 5K4-10 | Pressure | cheap, sturdy | Expensive | 383,07 dkk | https://dk.farnell.com/norgren/18-013-990/pressure-gauge-4bar/dp/7117991 |
| Pressure | 374-8878 | pressure switch | sturdy, usable for gasses as well as liquids | Expensive | 369,89 dkk | https://dk.rs-online.com/web/p/tryksensorer/3748878/ |
| Humidity | HCZ-D5-A | Humidity calculated based on Resistance | Cheap, Good specifications, compared to the price | Not that precise | kr.42,22 | https://dk.farnell.com/multicomp/hcz-d5-a/sensor-humidity-20-90-rh-5/dp/1891428 |
| Humidity | HYT 939 | Hygrochip | extremely sophisticated industrial applications, Calibrated and temperature compensated, Resistant to pressure up to 16bar | Very expensive | kr.419,14 | https://dk.farnell.com/ist-innovative-sensor-technology/hyt-939/sensor-humidity-digital-p-proof/dp/2191824 |
| Humidity | HPP801A031 | capacitive cell, Analogue | Full interchangeability with no calibration, required in standard conditions, Created in EU | Not very reliable and precise | kr 40,91 | https://dk.farnell.com/te-connectivity/hpp801a031/humidity-sensor-analogue-1-99/dp/3397836 |
