# Use Cases

![Use Cases Diagram](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/documentation/use_cases_diagram.png)

- ## Dashboard
    - Display Sensor settings
    - Select units of measure
    - Select time period
    - Display current temperature
    - Display statistics for time period
    - Display graph for time period
    - Display warnings<br><br>
- ## Program / Software
    - Get server status info
    - Get temperature at time
    - Get temperature for time period
    - Get single stat for time period
    - Get all stats for time period
    - Get current temperature
    - Send sensor data<br><br>

- ## Sensor Backend
    - Read Temperature
    - Read Humidity
