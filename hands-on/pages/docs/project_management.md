# Project Management

## How you worked with project management and scrum 
With project management we did it through having sprint planning meetings every second week, and scrum meetings every Monday and Wednessday.

At our Sprint planning meetings we would take a look at our GitLab board, going over that weeks exercises and update our GitLab board accordingly, creating **Product Backlog Items** (PBIs) and **Sprint Planning Item** (SPIs).
<br><br>
At our Scrum meetings we would also look at our GitLab board and go over the issues in:  **On Hold / Blocked**, **In progress** and **To-Do**
and get a status update from the ones who were assigned to the different issues, and also findout what we were going to work on that day.


## How you organized your gitlab projects
We ended up using our main gitlab project, instead of splitting it up into multiple projects inside of a GitLab group.
<br><br>
This made more sense for us at the time, since we didn't want the hazzle of having multiple repositories to keep track off and making sure to have them updated with the latest changes.

With our main gitlab repo, we made sure to have approtiate directories containg different parts of the project, such as:

- Documentation
- Hands-on (Exercises etc.)
- Images
- Meeting
- Node-RED
- Research


## Working with git and branching strategy

For working with Git and branching we chose the GitHub flow branching strategy, and we all commented on the benefits of it:

### Rudi's comments
While I personally find this project too small to be using branching for bigger projects with multiple, not necessarily interacting, parts it makes good sense for each team member to have their own branch, or version, of the project. This way each team member gets to work on their part of the project without interfering with the work of other members who are also doing their own thing. The project does however run the risk containing work done twice, which is why communication and regular updating of the master branch is important, so the team together can work on updating the base that is the master branch in a stable manner.

### Reuben's comments
The benefits of Git branching scale with the size of the group but can be useful even when working within small teams. Being able to edit files simultaneously makes the workflow much smoother. Using the Git branching removes a lot of the painful issues when using GitLab as part of a team such as worrying about other people editing the document at the same time and having merge conflicts when trying to commit, as you are resolving any merge conflicts before pushing any change to GitLab. In addition, having a team member reviewing any potential changes/conflicts adds an extra safety net.

### Jakob's comments
The two most important benefits of using git branching in my opinion is that it allows for multiple working on the same document at the same time, and that the team working on the project gets a better overview of the current branches of development currently being worked on, for example different features or bug fixes while still having a working master project.
<br><br>
Also, regarding merge requests, I find it useful that the changes can be reviewed and approved by a different set of eyes instead of only your own, before being merged into the master branch.


### Mathias's comments
The benefits I see for the git branching would mostly lie within working as a team and increasing work efficiency. By branching, you are creating your own little space, where you are free to add and remove files and make changes to code. You can develop mostly unhindered until it is time to merge, where you will have to make some compromises if any conflicts arise. Still, this would make it, so you will need to contact your team less, if you need to make a change to a separate part of the same working file. When merging, the number of tools available also makes sure that no weird commits or changes are merged before they have been analyzed. Enabling you to make comments on specific lines is useful for this as well.

### Christian's comments
The benefits of git branching is that you are able to do experiments with your project.
If we think of it as a sandbox, where you can try out some things, without harming the main project.

### Lauge's comments
I can see a clear benefit when working on larger projects where different teams must work on different parts independently, each team can make changes to specific parts while checking if the system is working in its own environment, then after all changes are done the result can be compiled.

## How you did the project plan
For the project plan, we had a template to start off with where we ended up with following topics:

- **Background**
	- Here we briefly explained what this project and project plan is about.
<br><br>
- **Purpose**
	- Here we explained the main goal/purpose of the project
<br><br>
- **Goals**
	- Here we showcased the expected output in the forms of a sensor system overview, and also what the different project delivieries were.
<br><br>
- **Schedule**
	- Here we linked to our lecture plan, which basically were our time schedule for this project.
<br><br>
- **Organization**
	- Here we listed our made-up organization consisting of a steering commitee, project managers, project groups and external stakeholders.
<br><br>
- **Budget and resources**
	- Here we came up with a somewhat budget consisting of our manpower we put into our project, financially we did not allocate any money towards this project. 
	<br><br>
	Sensors ended up being supplied by the school.
<br><br>
- **Risk assessment**
	- Here we had previously made a risk assessment for a previous project, so all of the previous risks were still valid.
<br><br>
- **Stakeholders**
	- Here we listed our made-up stakeholders, consisting of Us, Potential Customers, Teachers, Potential employers.
<br><br>
- **Perspectives**
	- Here we explained the perspectives of this project in regards on why we we are doing.
<br><br>
- **Evaluation**
	- Here we explained how we will go about the evaluation after the project is eventually done.
<br><br>
- **References**
	- Here we listed related references such as links to documentation, articles and etc.
<br><br>
- **Common project group on gitlab.com**
	- Here we linked to the main common project group that our project was situated in.
	
## How you did the pre mortem
The pre mortem was done on the background of our previous pre mortem done for our previous project, we went over the previous pre mortem by reviewing the current stated reasons and ended up adding new ones and updating the current.


## OME education research
### Learning goals
Acquire competences that allow them to take responsibility for operating and maintaining technical installations. 
They work with regards to safety, economical and environmental factors.

### Lifelong learning level
6

### Courses included in the education
An energy and environment special with maintanence opitmization
Automation and technical installations special
A maritime/seafaring special
a maritime idustrial/energy speical pertaining to the offshore branch
a technical project leader special.

### The kind of jobs OME’s occupy
Team leaders or managers on land, where they have responsibility for operations of larger machine installtions.
Work at powerplants, water filtrations plants, gas-, water and heating plants.
They can also end up working as counselors and advisors pertaining to selling and buying techincal advanced installations. 
Many cargo ships and their machine installations are purely automatica and only need a OME taking care of the maintanence and reparations.

### What challenges can we experience in our communication with OME’s
Working with sensors and creating sensor systems that connect and send information
to something like thingspeak so they can remotely check their phones. They probably dont know what our competences are right from the start.


## Project Milestones

### POC
#### Goals
- Working prototype
- General idea of the end project
- Finding suitable sensors for the system
- Having an actual idea of a possible solution (system)

### MVP
#### Goals
- A product with enough features to be able to operate as intended
- Several sensors measuring relevant data, using MQTT to send the data to a webserver / database

#### Purpose of MVP
- To check if the OME’s know what kind of product they want. 
- To test if the core system actually works in the required setting
- To show the simplest solution for measuring the aisles in the datacenter.

#### Features of the MVP
- Thermal data from a thermal sensor
- Warn the user when the temperature reaches a certain value.
- Publishing sensor data to a database
- Publishing database data to NodeRED

### Final adjustments and prepare presentations
#### Goals
- An actual functioning system with the right sensors to the clients specifications
- A viable product ready for market
- Prepare graphs and find the best way to relay the information
