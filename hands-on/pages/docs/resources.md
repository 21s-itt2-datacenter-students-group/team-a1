# Resources

## Gitlab group
[Click here to get redirected to our Gitlab group](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group)


## Course material
[Click here for the course material used for hands on, research, documention etc](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)


## Articles
[https://www.scrum.org/resources/what-is-scrum](https://www.scrum.org/resources/what-is-scrum)
<br>
[https://www.scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf](https://www.scrumguides.org/docs/scrumguide/v2020/2020-Scrum-Guide-US.pdf)
<br>
[https://www.scrum.org/resources/what-is-a-product-backlog](https://www.scrum.org/resources/what-is-a-product-backlog)
<br>
[https://www.scrum.org/resources/what-is-sprint-planning](https://www.scrum.org/resources/what-is-sprint-planning)
<br>
[https://www.scrum.org/resources/what-is-a-daily-scrum](https://www.scrum.org/resources/what-is-a-daily-scrum)
<br>
[https://www.scrum.org/resources/what-is-a-sprint-review](https://www.scrum.org/resources/what-is-a-sprint-review)
<br>
[https://www.scrum.org/resources/what-is-a-sprint-retrospective](https://www.scrum.org/resources/what-is-a-sprint-retrospective)
<br>
[https://www.scrum.org/resources/what-is-sprint-planning](https://www.scrum.org/resources/what-is-sprint-planning)
<br>
[https://www.scrum.org/resources/what-is-a-sprint-backlog](https://www.scrum.org/resources/what-is-a-sprint-backlog)
<br>
[https://hbr.org/2007/09/performing-a-project-premortem](https://hbr.org/2007/09/performing-a-project-premortem)
<br>
[https://en.wikipedia.org/wiki/Brainstorming](https://en.wikipedia.org/wiki/Brainstorming)
<br>
[https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-use-case-diagram/)
<br>
[https://www.eclipse.org/paho/index.php?page=clients/python/docs/index.php](https://www.eclipse.org/paho/index.php?page=clients/python/docs/index.php)
<br>
[http://www.steves-internet-guide.com/into-mqtt-python-client/](http://www.steves-internet-guide.com/into-mqtt-python-client/)
<br>
[https://www.fms.dk/](https://www.fms.dk/)
<br>
[https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester](https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester)
<br>
[https://ufm.dk/en/education/recognition-and-transparency/transparency-tools/qualifications-frameworks/levels?set_language=en&cl=en](https://ufm.dk/en/education/recognition-and-transparency/transparency-tools/qualifications-frameworks/levels?set_language=en&cl=en)
<br>
[https://opensource.com/resources/what-are-microservices](https://opensource.com/resources/what-are-microservices)
<br>
[https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/#iterations](https://about.gitlab.com/handbook/marketing/project-management-guidelines/milestones/#iterations)
<br>
[https://en.wikipedia.org/wiki/Burn_down_chart](https://en.wikipedia.org/wiki/Burn_down_chart)
<br>
[https://www.javatpoint.com/types-of-databases](https://www.javatpoint.com/types-of-databases)
<br>
[https://www.mongodb.com/nosql-explained](https://www.mongodb.com/nosql-explained)

#### [More can be found here](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)

## Official documentation
[https://pypi.org/project/paho-mqtt/](https://pypi.org/project/paho-mqtt/)
<br>
[https://docs.atlas.mongodb.com/getting-started/](https://docs.atlas.mongodb.com/getting-started/)

#### [More can be found here](https://eal-itt.gitlab.io/21s-itt2-project/exercises/)