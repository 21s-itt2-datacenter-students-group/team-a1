# Hardware

## Measuring techniques research
   
### Current

Open loop current sensors consist of a Hall sensor mounted in the air gap of a magnetic core (Fig. 1). A conductor produces a magnetic field comparable to the current. The magnetic field is concentrated by the core and measured by the Hall sensor. The signal from the Hall generator is low, so it is amplified, and it is this amplified signal that becomes the sensor’s output. Open-loop sensors normally have circuitry that provides temperature compensation and calibrated high-level voltage output. While they have a definite price advantage over closed-loop counterparts, their downside is that they can be prone to saturation and temperature drift. The drift can be minimized to some extent, however, by injecting a positive coefficient in the control current to reduce the drift in sensitivity over temperature.

- analog output, which duplicates the wave shape of the sensed current.
- bipolar output, which duplicates the wave shape of the sensed current.
- unipolar output, which is proportional to the average or RMS value of the sensed current.
- Hall sensor measures magnetic field of the current 
- useful to measure current without physically connecting to the system                               
- Can measure distance as well


### Temperature
#### Thermocouple
- A thermocouple is a sensor that consists of two dissimilar metal wires, joined at one end, and connected to a thermocouple thermometer or other thermocouple-capable device at the other end.
- When two wires composed of dissimilar metals are joined at both ends and one of the ends is heated, there is a continuous current which flows in the thermoelectric circuit
- when the junction of the two metals is heated or cooled a voltage is produced that can be correlated back to the temperature.
- 2-3 times cheaper than RTDs

#### Isobar
- Standard mercury Thermometer

#### Resistance Temperature Detectors (RTDs)
- RTDs work on a basic correlation between metals and temperature. As the temperature of a metal increases, the metal's resistance to the flow of electricity increases.
- Platinum is the most commonly used metal for RTD due to its chemical inertness, nearly linear temperature versus resistance relationship, temperature coefficient of resistance that is large enough to give readily measurable resistance changes with temperature and stability 

#### Thermistors
- A thermistor is a resistance thermometer, or a resistor whose resistance is dependent on temperature. The term is a combination of “thermal” and “resistor”. It is made of metallic oxides, pressed into a bead, disk, or cylindrical shape and then encapsulated with an impermeable material such as epoxy or glass.

### Humidity
- Capacitive, utilizing that a small change in humidity of the dielectric material causes a large change in capacitance to measure the humidity, cheap. durable but accuracy isn’t great. 
- Gravitational, most precise Hygrometer but expensive, impractical and hard to calibrate
- Resistance, measuring changing resistance in a salt or polymer caused by humidity, less accurate than capacitive and since the resistance also depends on the temperature needs to be combined with a thermometer
- Thermal, measures the thermal conductivity of the air as a function of humidity, measures absolute humidity.

### Airflow
- Mass flow sensor
- reverse fan?
- https://sps.honeywell.com/us/en/products/sensing-and-iot/sensors/airflow-sensors
- airflow sensors operating on heat transfer — flow and differential pressure
- air pressure(membrane)
- thermometers at key spots?


## Sensor requirement draft

### Potential requirement areas for sensors in a datacenter 

- Precise / accurate, high resolution range
- Doesn’t require daily calibrations to maintain accuracy
- Able to work under required temperature conditions (10 degrees - 80 degrees MAX)
- Waterproof / moisture proof (40%-60% relative humidity)
- Small enough to fit into tight places
- Complete modules (rather than creating the modules ourselves from the sensors)
- Uncertainty of measurement doesn’t exceed 5%
- Cheap enough to replace
- Power consumption


| Application | Location of sensors | Precision / accuracy |
| ------ | ------ | ------ |
| Ambient Room  Humidity | Depending on size of the room: close to the door, center of room, center of racks and furthest point from door. | 40% - 60% rH. (relative humidity) |
| Ambient Room Temperature. | small rooms: center. data centers: potential hot zones. | 18-27°C |
| Rack Level Monitoring Sensors to monitor intake temperature | Under data rack | 18-27°C / 64-80°F |
| Sensors to monitor outtake temperature | over data rack | no more than intake temperature + 20°C/K |


## List of possible sensors

### Available Suitable Sensors

| Type | Sensor | Measuring Technique | Pros | Cons | Cost | Link |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| Temperature | ZTP-135SR | measures temperature | Small size, Included thermistor for ambient comparison, High sensitivity, Fast response time, Low cost | its bad | 43,08 dkk | https://dk.farnell.com/amphenol-advanced-sensors/ztp-135sr/thermopile-ir-sensor-20-degree/dp/2506255 |
| Temperature | 287-18000 | Sensing | Cheap | Chinese Delivery time | 48,50 dkk | https://dk.farnell.com/mcm/287-18000/thermometer-type/dp/2830978 |
| Temperature | 44/830/7 | Isonbar | VERY CHEAP and Old people compatible | Physical, No digital data measurement | 14,72 dkk | https://dk.farnell.com/brannan/44-830-7/thermometer-glass-10-to-110deg/dp/3403902 |
| Temperature | 010010TD | Tubular ceramic encased Pt100 elements resistance dependant of temperature | High range, min and max temperature | Very expensive | 208,64 dkk | https://dk.farnell.com/labfacility/010010td/sensor-pt100-4wire-5x35mm/dp/7255743?st=temp |
| Temperature + Humidity | RHT 03 (SEN-10167) | capacitive humidity sensing digital temperature and humidity module | Includes a temperature and humidity sensor | Somewhat expensive, Ultra-low power, the transmission distance, fully automated calibration | 96,00 dkk | https://dk.farnell.com/mcm/83-17985/dht22-temp-humidity-sensor/dp/2801405?st=dht22 |
| Pressure | 5K4-10 | Pressure | cheap, sturdy | Expensive | 383,07 dkk | https://dk.farnell.com/norgren/18-013-990/pressure-gauge-4bar/dp/7117991 |
| Pressure | 374-8878 | pressure switch | sturdy, usable for gasses as well as liquids | Expensive | 369,89 dkk | https://dk.rs-online.com/web/p/tryksensorer/3748878/ |
| Humidity | HCZ-D5-A | Humidity calculated based on Resistance | Cheap, Good specifications, compared to the price | Not that precise | kr.42,22 | https://dk.farnell.com/multicomp/hcz-d5-a/sensor-humidity-20-90-rh-5/dp/1891428 |
| Humidity | HYT 939 | Hygrochip | extremely sophisticated industrial applications, Calibrated and temperature compensated, Resistant to pressure up to 16bar | Very expensive | kr.419,14 | https://dk.farnell.com/ist-innovative-sensor-technology/hyt-939/sensor-humidity-digital-p-proof/dp/2191824 |
| Humidity | HPP801A031 | capacitive cell, Analogue | Full interchangeability with no calibration, required in standard conditions, Created in EU | Not very reliable and precise | kr 40,91 | https://dk.farnell.com/te-connectivity/hpp801a031/humidity-sensor-analogue-1-99/dp/3397836 |





