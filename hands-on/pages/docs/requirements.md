# Requirements

## System brainstorm

What system will we be building?

- Small ESP8266 system
    - Special casing for the system?
    - Temperature sensor
    - Humidity sensor, Capacitive?
    - Dashboard to see data about temperature and humidity
        - Custom GUI?
        - Thingspeak / Thingspeak alternatives
        - Options of Graphic representation<br><br>
- A system to measure how effective the system is being cooled
    - Sensors measuring air temperature
    - Sensors measuring the water temp/ coolant temp
    - Sensors measuring the humidity in the air
    - measure temperature of radiator / reservoir / output
    - GUI for data
        - sends the data to thingspeak ever 15 secs
        - average temperature from each sensor
    - Measuring the effectiveness of the ventilation system by comparing the energy used by the system and the energy removed
    - Calculating if the ventilation is working by comparing intake and exhaust air temperatures<br><br>
- Small in the wall socket sensoring array(humidity and temperature) device
    - Sensors measuring airflow
    - IoT connection to correct problems in the making<br><br>
- Sensor for the liquid cooling?<br><br>
- Thermostat with a web camera pointed at it?
    - image analyzing code to have software automatically read off the thermostat
    - one of those regular interval scent sprayer, but it sprays water instead to keep the humidity up<br><br>
- Chilled water cooling with some sensors to measure?
- Make a sensor that can use the free air outside 
- Air cooling
- Measure fan RPM
- Measure temperature of vital components
- Measure temperature of ambient area
- Measure water temperature of brine
- Check temperature instantly
- Check fan RPM instantly
- Create IOT system for temperature and fans
- Make app for smartphone to check data
- Make LCD screens to check data
- Wireless LCD screens to check data
 

## Use cases

![Use Cases Diagram](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/documentation/use_cases_diagram.png)

### Dashboard
- Display Sensor settings
- Select units of measure
- Select time period
- Display current temperature
- Display statistics for time period
- Display graph for time period
- Display warnings<br><br>
### Program / Software
- Get server status info
- Get temperature at time
- Get temperature for time period
- Get single stat for time period
- Get all stats for time period
- Get current temperature
- Send sensor data<br><br>
### Sensor Backend
- Read Temperature
- Read Humidity

## Requirements draft

Requirements for system
### Hardware
- Temperature sensors
- Humidity sensors
- Raspberry Pi
- Cables / wires
- LEDs
- Screen to show data
- ESP8266
### Software
- SQLite
- Custom GUI (PyQt ?)
### Protocols:
- Coding Language
- Python
- SQL
- MQTT

