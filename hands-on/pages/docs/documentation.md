# Documentation

## Project plan
The entire project plan is very long so I wont be adding it to the pages. It can be found
[here](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/project_plan.md)

## Pre mortem
The premortem is part of the project plan and is called risk assesment.

The risk assesment/pre mortem can be found
[here](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/project_plan.md#risk-assessment)

## POC and MVP video’s
<iframe width="560" height="315" src="https://www.youtube.com/embed/q0jRaU8LtHU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/7gFmLQSLAN4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>