# Overview

## Introduction
This is the 2nd semester IT-Technology project where we will work with a data center project. Our pages will cover our part of the project, and will serve to collect and mediate knowledge we gained throughout our second semester.

Both classes are, together with the OMEs (Operational Machine Engineers), going to be creating a system to manage and survey the following:

* Cooling
* Power
* Ventilation

Our part will be taking care of cooling.

We will be working in close collaborations with the OMEs who are the ones creating the requirements for the system. They are experts at the technical requirements, while we will be filling their needs for sensors, measuring and displaying of the information they require to properly survey the system.

## Your system's purpose
The main goal is to have a system where IoT sensors collect data from a datacenter and display and store this information in relevant ways.
This is a learning project and the primary objective for us, is to get a good understanding of the challenges involved in making such a system and to give us a hands-on experience with the implementation.

## System overview
Shown below is our system block diagram, representing an overview of the system.

![](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/system_block_diagram.jpg)

## Screenshots from dashboard
![node_redsc1](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/node-redsc1.png)
This is the flowchart for our dashboard. Here the "MQTT in" nodes can be seen sending data to our functions described in detail in [software section](../software/#the-node-red-dashboard). The functions then send the data to the correct nodes, which can all interpret the data correctly and display time, values and charts that keep a history of the data.


![node_redsc2](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/raw/master/images/node-redsc2.png)

The dashboard is currently inactive and the chart depicting flow, is getting artifical data and is only to show the concept for actual implementation.

But you can see the offline status indicating that the last bit of info the dashboard got from the broker was that the associated sensor went offline.
