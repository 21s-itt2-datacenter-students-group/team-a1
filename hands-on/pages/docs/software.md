# Software

## MQTT and how we are using it
In this project we will be using MQTT or message que telemetry transmission to send our sensor data
from our Raspberry Pi, connected to our sensors to an online Broker running on a virtual machine on Microsoft Azure.
<br><br>
This broker can then be accesed using the IP adrress and topic, which allows us to store the data in a seperate database and also shown on sensor reading on a dashboard for the Operational maintenence engineers.
The main advantage of using MQTT is that its a very lightweight communication standard so its ideal for wireless transmissions.    

## Sensor data via. RPi and MQTT solution code
### [Click here to see the entire Python code solution for our system](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/tree/master/hands-on/code)

## Data persistence using MongoDB
### [Click here to see the MQTT-To-MongoDB Python code for our system](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/code/ciots_subscriber.py)


## The Node-red dashboard
For the node-red dashboard we used javascript functions to enable our charts and gauges to correctly recieve and display information.
```
msg1 = msg
msg2 = msg

if(msg.topic.includes("sensor_1"))
{
    msg1.timestamp = msg.payload.timestamp
    msg1.payload = msg.payload.temperature.toFixed(2)
    msg2 = null
}
else if(msg.topic.includes("sensor_2"))
{
    msg2.timestamp = msg.payload.timestamp
    msg1.payload = (msg.payload.temperature + Math.random()).toFixed(2)
    msg1 = null
}

return [msg1, msg2]
```
This code is for the first function that takes info from an MQTT in node and delivering the data to our charts and gauges. The code makes sure that if two sensors are sending to the same "MQTT in" node, that it can differentiate the two inputs and correctly implement them.

This makes it convoluted, because if you would you like to add more sensors. You would essentially need to rewrite parts of the code instead of it dynamically adding sensors and naming them. This requires some thinking and teamwork from both the OME and the IT-Technology students. 


```
msg1 = msg
msg2 = msg

if (msg.payload.includes("is not running"))
{
    msg1.payload = "Offline"
    msg2.payload = "Offline"
}

else if(msg.payload.includes("sensor_1"))
{
    if (msg.payload.includes("online"))
    {
        msg1.payload = "Online"
    }
    else if(msg.payload.includes("offline"))
    {
        msg1.payload = "Offline"
    }
    else
    {
        msg1 = null
        msg2 = null
    }
    msg2 = null
}

else if(msg.payload.includes("sensor_2"))
{
    if (msg.payload.includes("online"))
    {
        msg2.payload = "Online"
    }
    else if(msg.payload.includes("offline"))
    {
        msg2.payload = "Offline"
    }
    else
    {
        msg1 = null
        msg2 = null
    }
    msg1 = null
}

return [msg1, msg2]
```

This code is for the "operating status" text field to correctly update if the sensor goes offline. A seperate MQTT in node is setup, that listens purely on a "last will" topic. A "last will" is a final text, that the broker will handle sending if a connected MQTT client loses connection unexpected. The will can be set in the MQTT client code, when you first connect. 

This "will" can be anything, but we have had our code make distinctions between two sensors and update the correct one's status if connection is lost. 
