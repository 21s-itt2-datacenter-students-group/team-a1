from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random
import json
sensorvalue = 0
payload = "The code is working"

# variables
sensor_id = 42
User = 'Sin9ularity'
broker_address = '52.170.26.171'  
broker_port = 1883
topicSub = 'Reuben/#'
topicPub = 'Reuben/Test'
publish_interval = 1

message = "The code works"

client = mqtt.Client(User) #init client

# triggers when message received from broker
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'connected to {broker_address} on port {broker_port} ')


# read the sensor
def read_sensor():
    sensor_reading = random.randint(0,10)
    return sensor_reading


# create payload
def build_payload(sensorvalue):
    sensorvalue = read_sensor()
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    payload = {'User': User, 'Subscribed to the topic': topicPub, 'using the sensor_id': sensor_id, 'sensor_time': timestamp, 'sensor_value(random int)': sensorvalue, 'message': message}
    return json.dumps(payload) # convert to json format


# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

try: 
    print('connecting to broker')
    client.connect(broker_address, broker_port)
    print(f'connected to {broker_address} on port {broker_port} ')
    client.subscribe(topicSub)
    print(f'Subscribed to topics: {topicSub}')
    client.loop_start() #start the loop

except socket_error as e:
    print(f'could not connect {User} to {broker_address} on port {broker_port}\n {e}')
    exit(0)

# publish
while True:
    try:
        payload = build_payload(sensorvalue)
        print(f'Publishing {payload} to topic, {topicPub}')
        client.publish(topicPub, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: 
        client.loop_stop() 
        exit(0)
