import paho.mqtt.client as mqtt
from random import randrange
from time import sleep

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("jbrd99/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("message received " ,str(msg.payload.decode("utf-8")))
    print("message topic=",msg.topic)
    print("message qos=",msg.qos)
    print("message retain flag=",msg.retain)
    print("\n")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message


client.connect("13.94.98.168",1883,10)

client.loop_start() #start the loop

try:
    while True:

        randNum = randrange(0,10)

        # msg = f''' 
        # .       .\n
        # |\_---_/|\n
        # /   o_o  \\\n
        # |    U    |\n
        # \  ._{randNum}_.  /\n
        # `-_____-'\n
        # '''

        
        msg = f"Generated random number: {randNum}" 

        client.publish("jbrd99/spam",msg)

        sleep(1)
except KeyboardInterrupt:
    client.loop_stop()

