from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random
import json

# variables
sensor_id = 5500
client_name = 'Mathias Knudsen'
broker_addr = '13.68.236.117'  
broker_port = 1883
topicSub = 'mhjk/#'
topicPub = 'mhjk/test'
publish_interval = 5

client = mqtt.Client(client_name) #init client

# triggers when message received from broker
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'connected to {broker_addr} on port {broker_port} ')

# create payload
def build_payload(sensor_reading, sensor_id):
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    payload = {'client': client_name, 'topic': topicPub, 'sensor_id': sensor_id, 'sensor_time': timestamp, 'sensor_value': sensor_reading}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
    sensor_reading = random.randrange(0,10)
    return sensor_reading

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

try: 
    print('connecting to broker')
    client.connect(broker_addr, broker_port)
    print(f'connected to {broker_addr} on port {broker_port} ')
    client.subscribe(topicSub)
    print(f'Subscribed to topics: {topicSub}')
    client.loop_start() #start the loop

except socket_error as e:
    print(f'could not connect {client_name} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:    
        payload = "Testing monke\n"
        print(f'Publishing {payload} to topic, {topicPub}')
        client.publish(topicPub, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)
