from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
import datetime
import random
import json

# variables
sensor_id = 5500
client_name = 'Mathias_Knudsen'
broker_addr = 'mhjk28842.eastus.cloudapp.azure.com'  
broker_port = 1883
topicSub = 'mhjk/#'
topicPub = 'mhjk/spam'
publish_interval = 2
now = datetime.datetime.now()

client = mqtt.Client(client_name) #init client

# triggers when message received from broker
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'connected to {broker_addr} on port {broker_port} ')

# create payload
def build_payload():
    payload = {"abe" : "Hello there" + currtime}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
    sensor_reading = random.randrange(0,10)
    return sensor_reading

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

try: 
    print('connecting to broker')
    client.connect(broker_addr, broker_port)
    print(f'connected to {broker_addr} on port {broker_port} ')
    client.subscribe(topicSub)
    print(f'Subscribed to topics: {topicSub}')
    client.loop_start() #start the loop

except socket_error as e:
    print(f'could not connect {client_name} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:
        now = datetime.datetime.now()
        currtime = now.strftime("%H:%M:%S")
        payload = build_payload()
        print(f'Publishing {payload} to topic, {topicPub}')
        client.publish(topicPub, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)
