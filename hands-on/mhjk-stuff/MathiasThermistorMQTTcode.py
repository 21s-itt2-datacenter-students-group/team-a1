'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | february 2021
modified by: Mathias Knudsen
'''

from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import math
import random
import json
import RPi.GPIO as GPIO
from ADCDevice import *

# variables
adc = ADCDevice()
sensor_id = 4200
client_name = 'Jakob_sucks_cock'
broker_name = 'broker.hivemq.com'  
#broker_name = 'nisimqtt.germanywestcentral.cloudapp.azure.com'
broker_port = '1883'
topicSub = 'teamA1/#'
topicPub = 'teamA1/sensors/mkTermo'
publish_interval = 1
client = mqtt.Client(client_name, broker_port) #init client

# received message callback
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))
    # print('message topic=',message.topic)
    # print('message qos=',message.qos)
    # print('message retain flag=',message.retain)

client.on_message=on_message # attach message callback to callback

# create payload
def build_payload(sensor_reading, sensor_id):
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    payload = {'sensor_id': sensor_id, 'sensor_time': timestamp, 'sensor_value': sensor_reading}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
    value = adc.analogRead(0)        # read ADC value A0 pin
    voltage = value / 255.0 * 3.3        # calculate voltage
    Rt = 10 * voltage / (3.3 - voltage)    # calculate resistance value of thermistor
    tempK = 1/(1/(273.15 + 25) + math.log(Rt/10)/3950.0) # calculate temperature (Kelvin)
    tempC = tempK -273.15        # calculate temperature (Celsius)
    sensor_reading = tempC
    print ('ADC Value : %d, Voltage : %.2f, Temperature : %.2f'%(value,voltage,tempC))
    time.sleep(0.01)
    return sensor_reading

# Sensor code
def setup():
    global adc
    if(adc.detectI2C(0x48)): # Detect the pcf8591.
        adc = PCF8591()
    elif(adc.detectI2C(0x4b)): # Detect the ads7830
        adc = ADS7830()
    else:
        print("No correct I2C address found, \n"
        "Please use command 'i2cdetect -y 1' to check the I2C address! \n"
        "Program Exit. \n")
        exit(-1)


try: 
    setup()
    print('connecting to broker')
    client.connect(broker_name)
    print(f'connected to {broker_name} on port {broker_port} ')

except socket_error as e:
    print(f'could not connect {client_name} to {broker_name} on port {broker_port}\n {e}')
    exit(0)

# subscribe
client.loop_start() #start the loop
client.subscribe(topicSub)
print(f'Subscribed to topics: {topicSub}')

# publish
while(True):
    try:    
        payload = build_payload(read_sensor(), sensor_id)
        print(f'Publishing {payload} to topic, {topicPub}')
        client.publish(topicPub, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        GPIO.cleanup()
        adc.close()
        exit(0)
