from random import uniform
from statistics import mean
from collections import deque

real_val = 4
max_noise = 1
res = list()
resErrs = list()

def getNoisy(real_val, max_noise):
    noise = uniform(-1 * max_noise, max_noise)
    output = real_val + noise
    return output

def getMean(list_data_vals):
    mean_data_vals = mean(list_data_vals)
    return mean_data_vals

buffer = deque(maxlen=5)

## Filling buffer
for i in range(5):
    buffer.append(getNoisy(real_val,max_noise))

for i in range(20):
    buffer.append(getNoisy(real_val,max_noise))
    meaned = getMean(buffer)
    res.append(meaned)

for meaned in res:
    resErrs.append(abs(meaned - real_val))

print(getMean(resErrs))