from random import uniform
from statistics import mean
from collections import deque

res = list()
resErrors = list()
real_val = 23.5
max_noise = 1
buffer = deque(maxlen=5)

def get_noisy(real_val, max_noise):
    noise = uniform(-1 * max_noise, max_noise)
    output = real_val + noise
    return output

def get_mean(vals):
    return round(mean(vals),2)

## FILLING THE BUFFER
for i in range(5): 
    val = get_noisy(real_val,max_noise)
    buffer.append(val)

## ADDING TO THE BUFFER
for i in range(20):
    val = get_noisy(real_val,max_noise)
    print("Adding value: %f to buffer" % (val))
    buffer.append(val)
    mean_val = get_mean(buffer)
    print("Mean is:",mean_val)
    res.append(mean_val)

for index, item in enumerate(res):
    print("Reading value %d in res: %f" %(index, item))

for item in res:
    resErrors.append(abs(item - real_val))

print("\nMean error is: ", get_mean(resErrors))