import Adafruit_DHT
import paho.mqtt.client as mqtt
import json
import pymongo
from time import sleep, time
from datetime import datetime
from socket import error as socket_error
import functions

# variables
sensor_id = 6549
client_name = 'itt_teamA1_sensordht11'

#mqtt_broker_addr = 'test.mosquitto.org'
mqtt_broker_addr = 'broker.hivemq.com'
#mqtt_broker_addr = '13.94.98.168'
mqtt_broker_port = 1883
topic = 'sensordata/sensor_2'
publish_interval = 0.5
client = mqtt.Client(client_name) #init client
dht_sensor = Adafruit_DHT.DHT11
dht_pin = 17
dbUser = "dbAdmin"

token = functions.get_token('/home/pi/Desktop/2_semester/team-a1/hands-on/token.txt')

db_client = pymongo.MongoClient(f"mongodb+srv://dbAdmin:{token}@cluster0.xxw3w.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = db_client.mqtt_test
db_collection = db.dht11

def on_connect(client, userdata, flags, rc):
    print(f'connected to {mqtt_broker_addr} on port {mqtt_broker_port} ')

def on_message(client, userdata, message):
    document = json.loads(message.payload) # create a dictionary from MQTT received message
    print(f'attempting db store: {document}') # print the document for development purposes
    # return_id = db_collection.insert_one(document) # insert the document in the database
    print(f'stored in db with _id: {return_id.inserted_id}') # print the returned document _id from the database

client.on_message = on_message # attach message callback to callback
client.on_connect = on_connect
client.loop_start()

# create payload
def build_payload(sensor_readings, sensor_id):
    #timestamp = int(datetime.utcnow().timestamp()) * 1000 # utc timestamp
    timestamp = int(time() * 1000)
    payload = {'temperature': sensor_readings[0], 'humidity': sensor_readings[1],'timestamp':timestamp}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
   humidity, temperature = Adafruit_DHT.read(dht_sensor, dht_pin)
   if humidity is not None and temperature is not None:
       return temperature, humidity
   else:
       return None


try: 
    print('connecting to broker')
    client.connect(mqtt_broker_addr, mqtt_broker_port)
    sleep(2)
except socket_error as e:
    client.loop_stop()
    print(f'could not connect {client_name} to {mqtt_broker_addr} on port {mqtt_broker_port}\n {e}')
    exit(0)




# publish
while(True):
    try:
        data = read_sensor()
        if data != None:    
            payload = build_payload(data, sensor_id)
            print(f'Publishing {payload} to topic, {topic}')
            client.publish(topic, payload)        
            # sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)



