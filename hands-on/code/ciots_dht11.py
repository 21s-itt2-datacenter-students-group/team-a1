try:
    import Adafruit_DHT as dht
except:
    pass
from statistics import mean
from collections import deque
from time import sleep

class CIoTSDHT11():
    
    def __init__(self, red_led):
        self.dht = dht
        self.dht_sensor = dht.DHT11
        self.dht_pin = 17
        self.red_led = red_led

    # read the sensor values
    def read_sensor(self):
        attempt_cnt = 0
        while True:
            humidity, temperature = self.dht.read(self.dht_sensor, self.dht_pin)

            if humidity is not None and temperature is not None:
                if attempt_cnt >= 1:
                    attempt_cnt = 0
                self.red_led.off()
                return {"temperature":temperature, "humidity":humidity}
            else:
                attempt_cnt += 1
                print("\nCould not retrieve sensor data - attempting again!\nAttempt:", attempt_cnt)

            if attempt_cnt >= 29:
                self.red_led.blink(0.2,0.2,5)
                self.client.publish(self.status_topic,payload=self.client_name + ": Could not retrieve sensor data!")
                return None
    
    def get_buffered_value(self, samples, buffersize):
        temp_buffer = deque(maxlen=buffersize)
        humd_buffer = deque(maxlen=buffersize)

        ## Filling the buffer
        for i in range(samples):
            data = self.read_sensor()
            print("\nAdding temperature value: %f to buffer" % (round(data["temperature"],2)))
            temp_buffer.append(round(data["temperature"],2))
            print("\nAdding humidity value: %f to buffer" % (round(data["humidity"],2)))
            humd_buffer.append(round(data["humidity"],2))
            sleep(2)
        ## Calculating mean of buffer
        
        buffer_data = {"temperature:":mean(temp_buffer), "humidity":mean(humd_buffer)}
        return buffer_data