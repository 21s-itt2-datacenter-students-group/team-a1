#!/usr/bin/python
from time import sleep
from ciots_client import CIoTSClient
from ciots_dht11 import CIoTSDHT11
from ciots_fcs01 import CIoTSFCS01

class CIoTSPublisher(CIoTSClient):

    def __init__(self, client_name):
        super().__init__(client_name)


if __name__ == '__main__':

    try:
        publisher = CIoTSPublisher("sensor_1")

        
        publisher.client.on_connect = publisher.on_connect
        publisher.client.on_disconnect = publisher.on_disconnect

        publisher.connect_to_broker()
        publisher.client.loop_start()

        publisher.dht11 = CIoTSDHT11(publisher.red_led)
        publisher.fcs01 = CIoTSFCS01()

        while True:
            data = publisher.read_sensors()
            if data != None:    
                payload = publisher.build_payload(data)
                print(f'Publishing {payload} to topic, {publisher.topic}')
                publisher.client.publish(publisher.topic, payload)        
                sleep(publisher.publish_interval) # wait x seconds

    except Exception as e:
        print(e)
        publisher.client.disconnect()
        publisher.client.loop_stop() #stop the loop
        exit(0)



