from sys import path as syspath
from os import path as ospath
from socket import error as socket_error
from json import loads, dumps as jsondumps, JSONDecodeError
from time import time
from pymongo import MongoClient
import ssl


class CIoTSTools:

    def connect_to_broker(self):
        try: 
            print("connecting to broker")
            self.client.connect(self.mqtt_broker_addr, self.mqtt_broker_port)
            self.client.will_set("ciots/status", payload=self.client_name + " went offline unexpectedly!", qos=0, retain=True)
        except socket_error as e:
            self.client.loop_stop()
            print(f"could not connect {self.client_name} to {self.mqtt_broker_addr} on port {self.mqtt_broker_port}\n {e}")
            exit(0)


    def on_connect(self, client, userdata, flags, rc):
        print(f'connected to {self.mqtt_broker_addr} on port {self.mqtt_broker_port} ')
        client.publish(self.status_topic,payload=self.client_name + " is now online!")


    def on_disconnect(self, client, userdata, rc):
        if rc != 0:
            print("Unexpected disconnection!")
            self.red_led.blink(0.2,0.2,5)
        else:
            client.publish(self.status_topic,payload=self.client_name + " is now offline!")


    def on_message(self, client, userdata, message):
        try:
            document = loads(message.payload) # create a dictionary from MQTT received message
            print(f'attempting db store: {document}') # print the document for development purposes
            return_id = self.db_collection.insert_one(document) # insert the document in the database
            print(f'stored in db with _id: {return_id.inserted_id}') # print the returned document _id from the database
        except JSONDecodeError:
            print("Error: message was not formatted as JSON")


    def on_subscribe(self, client, userdata, mid, granted_qos):
        print(f"Succesfully subscribed to f{self.topic}")


    def enable_tls(self, ca_cert, client_crt, client_key):
        self.client.tls_set(ospath.join(syspath[0], ca_cert), certfile=ospath.join(syspath[0], client_crt), keyfile=ospath.join(syspath[0], client_key), tls_version=2)
        self.client.tls_insecure_set(True)


    def read_sensors(self):
        tmp_payload = self.dht11.get_buffered_value(30, 5)
        for key, value in self.fcs01.read_sensor().items():
            tmp_payload[key] = value
        return tmp_payload

    # create payload
    def build_payload(self, sensor_readings):
        timestamp = int(time() * 1000)
        payload = {'sensor_id': self.client_name, 'timestamp':timestamp}
        for key, value in sensor_readings.items():
            payload[key] = value
        return jsondumps(payload) # convert to json format

    
    # open and strip token
    def get_token(self, filename):
        with open(ospath.join(syspath[0], filename), 'r') as f:
            token = f.readline()
        return token.strip()


    def mqtt_to_db(self):
        self.token = self.get_token('token')
        self.dbUser = "dbAdmin"

        self.db_client = MongoClient(f"mongodb+srv://{self.dbUser}:{self.token}@cluster0.xxw3w.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        self.db = self.db_client.mqtt_test
        self.db_collection = self.db.dht11

        