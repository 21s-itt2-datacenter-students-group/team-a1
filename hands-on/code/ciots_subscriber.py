from time import sleep, time
from datetime import datetime
from ciots_client import CIoTSClient

class CIoTSSubscriber():

    def __init__(self, client_name):
        super().__init__(client_name)


if __name__ == '__main__':
        
    try:
        subscriber = CIoTSSubscriber(client_name = "MQTTtoDB")

        subscriber.client.on_connect = subscriber.on_connect
        subscriber.client.on_message = subscriber.on_message
        subscriber.client.on_subscribe = subscriber.on_subscribe

        subscriber.connect_to_broker()
        subscriber.client.loop_start()
        subscriber.client.subscribe(subscriber.topic)
        while True:
            pass
    except:
        subscriber.client.loop_stop() #stop the loop
        exit(0)



