from ciots_client import CIoTSClient
from ciots_dht11 import CIoTSDHT11
from ciots_fcs01 import CIoTSFCS01
from ciots_overwatch import CIoTSOverwatch
from ciots_publisher import CIoTSPublisher
from ciots_subscriber import CIoTSSubscriber
from ciots_tools import CIoTSTools
import unittest

class TestClient(unittest.TestCase):
    
    def test_publisher_instantiation(self):
        obj = CIoTSClient("mongodb_subscriber_client")
        self.assertIsNotNone(obj)
    
    def test_subscriber_instantiation(self):
        obj = CIoTSClient("sensor_publisher_client")
        self.assertIsNotNone(obj)

class TestOverwatch(unittest.TestCase):
    
    ## This also tests the instantiation of the CIoTSOverwatch class
    def test_check_status(self):
        obj = CIoTSOverwatch("ciots_overwatch_client")
        output = obj.check_status()
        self.assertEqual(type(output), bool)
    
if __name__ == '__main__':
    unittest.main()