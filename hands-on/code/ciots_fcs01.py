class CIoTSFCS01():

    # read the sensor values
    def read_sensor(self):
        if self.fcs_01.is_pressed:
            print("Water is flowing!")
            return {"Start pipe": True}
        else:
            print("Water is not flowing!")
            return {"Start pipe": False}