import paho.mqtt.client as mqtt
from uuid import uuid4
from ciots_tools import CIoTSTools

class CIoTSClient(CIoTSTools):

    ## If sensor_name is filled out, publisher config is chosen or set as blank for subscriber config
    def __init__(self, client_name = "N/A", broker_addr = "public.mqtthq.com", broker_port = 1883):
        self.client_name = client_name + "_" +uuid4().hex
        self.client = mqtt.Client(self.client_name)
        self.mqtt_broker_addr = broker_addr
        self.mqtt_broker_port = broker_port
        self.status_topic = "ciots/status"
        
        ## Subscriber
        if "sensor" not in client_name: 
            self.topic = "ciots/sensors/#"
            self.mqtt_to_db()

        ## Publisher
        elif "sensor" in client_name: 
            self.topic = "ciots/sensors/" + self.client_name
            self.publish_interval = 2
            try: 
                from gpiozero import LED, Button
                self.red_led = LED(26)
                self.green_led = LED(19)
                self.fcs_01 = Button(21)
            except:
                pass
        
        ## Watcher
        else:
            pass 