import paho.mqtt.client as mqtt
import subprocess
from time import sleep
from ciots_client import CIoTSClient

class CIoTSOverwatch(CIoTSClient):

    def __init__(self, client_name):
        super().__init__(client_name)

    def check_status(self):
        process = subprocess.Popen("ps aux | grep ciots_publisher",
                                    shell=True,
                                    stdout=subprocess.PIPE,
                                )
        output = process.communicate()[0].decode("utf-8")
        if "python ciots_publisher.py" in output:
            return True
            print("CIoTSPublisher is running!")
        else:
            print("CIotSPublisher is not running!")
            return False

if __name__ == '__main__':
    try:
        watcher = CIoTSOverwatch("ciots_overwatch")
        watcher.client.connect(watcher.mqtt_broker_addr, watcher.mqtt_broker_port)
        watcher.client.loop_start()
        while True:
            if watcher.check_status() == False:
                watcher.client.publish(watcher.status_topic, "Publisher program is not running!")
            sleep(15)
    finally:
        watcher.client_disconnect()
        watcher.client.loop_stop()