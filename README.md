# [Team A1](https://21s-itt2-datacenter-students-group/team-a1)

### Team Members 
| First name | Last name | Username |
| ----- | ----- | ----- |
| Jakob | Dahl | @JBRD99 |
| Christian | Dalsgard | @chrid1909 |
| Lauge | Mathiesen | @lauge.mat |
| Mathias | Knudsen | @mhjk28842 |
| Rudi | Hansen | @rohh28794 |
| Reuben | Badham | @Reuben_Badham |


# Project: Datacenter IoT 

- [Team A1 Website](https://21s-itt2-datacenter-students-group.gitlab.io/team-a1/overview/)

- [Project plan with block diagram](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/project_plan.md)

- [Pre mortem](about:blank)

- [System Brainstorm - What system will we be building?](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/meeting/brainstorm.md)

- [System analysis - use cases](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/use_cases.md)

- [Requirements](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/requirements.md)

- [MQTT introduction solution code](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/mqtt_jbrd99.py)

- [OME education research](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/research/research_OME.md)

- [Measuring techniques research](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/sensor-measuring-techniques.md)

- [Potential requirements areas for sensors](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/potential_sensor_requirements.md)

- [Available Suitable Sensors](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/available_suitable_sensors.md)

- [Sensor data via RPi and MQTT](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/tree/master/hands-on)
    - [Jakob Dahl - DHT11](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/dht11_mqtt.py)
    - [Mathias Knudsen - Thermistor](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/MathiasThermistorMQTTcode.py)
    - [Lauge Mathiesen - ?](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/publishmqtt.py)
    - [Reuben Badham - Random int](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/hands-on/reubenpublishmqtt.py)<br><br> 
- [Role Rotation](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/role_Rotation.md)
- [Milestones](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/milestones/)
    - [Proof of concept (POC)](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/milestones/1)
    - [Minimum viable product (MVP)](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/milestones/2)
    - [Final adjustments and prepare presentations](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/milestones/3)<br><br>
- [MQTT Recreation](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/mqtt_recreation.md)

- [POC 3 min Video](https://www.youtube.com/watch?v=q0jRaU8LtHU)

- [Subgroup - projects](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group)
    - [MQTT](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group/mqtt/-/blob/master/README.md)
    - [Node RED](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group/node-red/-/blob/master/README.md)
    - [Datacenter](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group/datacenter/-/blob/master/README.md)
    - [Documentation](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group/documentation/-/blob/master/README.md)<br><br> 
- [Branching strategy](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1-group/documentation/-/blob/master/branching_strategy.md)
- [User testing process](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/research/user_testing_methods.md)
- [MVP 3 min Video](https://youtu.be/7gFmLQSLAN4) 

- [Security for MQTT](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/documentation/security_for_mqtt.md)
- [Business understanding](https://gitlab.com/21s-itt2-datacenter-students-group/team-a1/-/blob/master/research/business_understanding.md)
