## In your team decide which user testing method(s) you are going to use.
- Surveys (Jakob)
- Beta testing (Jakob, Lauge, -Reuben)
- Focus Group(Rudi)
- A/B testing (Lauge)
 
## Decide how you are going to observe and document your observations of the user tests.
- record? Turn the recording into written format. summary.
- Take notes of the meeting. 
- written responses in form of emails or similar
- Record how OME’s interact with the dashboard and respond to different values occurring.
 
## Decide on how you are going to evaluate the test results?
- If you are using a video recording it will take a long time to evaluate the results, if you are using a guide with questions it might be quick to - evaluate results. Think about this when deciding.
- Questionarie/survey maybe to get their opinions?
- If OME’s understand our outlay and have easy access to the data it's a success.
 
## Ask your users to participate in the test and arrange a fixed date and time
- N/A
