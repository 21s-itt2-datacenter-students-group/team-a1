# Business understanding

## 1. Similar companies
```
Size of the company.
Their target customer - B2B or B2C?, What industry area is targeted?, etc.
How do they make money - Selling a product?, Selling a service?, etc.
```



| Name | Company size | Target customer | Source of income |
| ------ | ------ | ------ | ------ |
| Delta Data Center Infrastructure Solutions | Unknown (Large) | B2B - Data Centers | Provides power management products and data center infrastructure solutions |
| ARANER Group | 51-200 employees | B2B - Data Centers | Provides a wide range of products, solutions and professional services to clients all across the world, where its leading-edge capabilities cover several disciplines, mostly linked to Energy systems and Cooling and Heating plants. |
| TDK | Global (21600 employs) | B2B and B2C - It systems and Electrical components, consumer electronics | Provides consumer and industrial components and power supplies, infrastructure solutions |

## 2. Company types in Denmark
```
Research the following company types present in Denmark:
IVS – Entrepreneur company
ApS – Private Limited Company
A/S – Public Limited Company
```


We chose: **ApS – Private Limited Company**
- **Pros**
    - Limited loss (Minimum 40.000 DKK or whatever you put in)
    - Lower start capital then A/S
    - More serious towards investors and customers
    - Proper accounting
    - Option to transfer profits to low or no tax countries using holding companies 
    - Multiple owners (More competencies and share the responsibility)
- **Cons**
    - Accounting will be taking care of looking at the financial statements
    - Start up cost
    - Cost of hiring an accountant
    - Audits
    - Requires a hierarchy 

 
## 3. Organisational chart
Draw up an organisational chart for your fictional company.
![](images/Project_Fictional_Company_Chart.png)
